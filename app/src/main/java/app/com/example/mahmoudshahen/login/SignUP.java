package app.com.example.mahmoudshahen.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUP extends AppCompatActivity {

    EditText emailEditText, passEditText, nameEditText, numberEditText,
            birthDateEditText;
    Button signUp;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Firebase.setAndroidContext(this);
        firebaseAuth = FirebaseAuth.getInstance();

        // tie with UI
        emailEditText = (EditText)findViewById(R.id.newEmail);
        passEditText = (EditText)findViewById(R.id.newPass);
        numberEditText = (EditText)findViewById(R.id.number);
        birthDateEditText =(EditText)findViewById(R.id.birthDate);
        nameEditText =(EditText)findViewById(R.id.name);
        signUp = (Button)findViewById(R.id.newAccount);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
    }
    private void registerUser(){

        //getting email and password from edit texts
        final String[] emails = emailEditText.getText().toString().split(" ");
        emails[0] = emails[0].substring(0, emails[0].length()-4);
        final String email = "\""+emails[0]+"\"";
        final String password  = passEditText.getText().toString().trim();
        final String name  = nameEditText.getText().toString().trim();
        final String number  = numberEditText.getText().toString().trim();
        final String birth  = birthDateEditText.getText().toString().trim();

        //checking if email and passwords are empty
        if(TextUtils.isEmpty(email)){
            Toast.makeText(this, "Please enter email", Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(password)){
            Toast.makeText(this, "Please enter password", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(name)){
            Toast.makeText(this, "Please enter name", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(number)){
            Toast.makeText(this, "Please enter number", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(birth)){
            Toast.makeText(this, "Please enter birthdate", Toast.LENGTH_LONG).show();
            return;
        }

        //if the email and password are not empty
        //displaying a progress dialog

        progressDialog = ProgressDialog.show(this, "Please Wait...", "Processing...", true);
        Log.v("done", email+" "+password+" "+name+" "+number+" "+birth);
        //creating a new user
        firebaseAuth.createUserWithEmailAndPassword(emails[0]+".com", password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if (task.isSuccessful()) {
                            //display some message here
                            Toast.makeText(SignUP.this, "Successfully registered", Toast.LENGTH_LONG).show();
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference databaseReference = database.getReference(email);

                            Data data = new Data();

                            data.setName(name);
                            data.setBirthDate(birth);
                            data.setNumber(number);

                            databaseReference.setValue(data);
                            Intent intent = new Intent(SignUP.this, Profile.class);
                            intent.putExtra("email", email);
                            startActivity(intent);

                        } else {
                            //display some message here
                            Toast.makeText(SignUP.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    }
                });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}
