package app.com.example.mahmoudshahen.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {
    EditText emailEditText, passEditText;
    Button login;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;
    CheckBox remember, showPass;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Firebase.setAndroidContext(this);
        firebaseAuth = FirebaseAuth.getInstance();
        emailEditText = (EditText)findViewById(R.id.Email);
        passEditText = (EditText)findViewById(R.id.Password);
        remember = (CheckBox)findViewById(R.id.remember);
        showPass = (CheckBox)findViewById(R.id.showPass);

        login = (Button)findViewById(R.id.Login);
        sharedPreferences = getSharedPreferences("file", 0);
        editor = sharedPreferences.edit();
        boolean isChecked = sharedPreferences.getBoolean("checked", false);
        if(isChecked)
        {
            emailEditText.setText(sharedPreferences.getString("email", ""));
            passEditText.setText(sharedPreferences.getString("pass", ""));
            remember.setChecked(true);
        }
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
                if (remember.isChecked())
                {
                    editor.putString("email", emailEditText.getText().toString());
                    editor.putString("pass", passEditText.getText().toString());
                    editor.putBoolean("checked", true);
                    Log.v("done", "done");
                }
                else
                {
                    editor.remove("checked");
                    editor.remove("email");
                    editor.remove("pass");
                }
                editor.commit();
            }
        });
        showPass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    passEditText.setInputType(129);
                } else {
                    passEditText.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
    }

    private void loginUser()
    {
        final ProgressDialog progressLogin = ProgressDialog.show(this, "Please Wait...", "Processing...", true);
        final String []email = emailEditText.getText().toString().split(" ");
        String pass = passEditText.getText().toString();

        Log.v("done", email[0] + "   " + pass);
        firebaseAuth.signInWithEmailAndPassword(email[0], pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressLogin.dismiss();
                if (task.isSuccessful()) {
                    Toast.makeText(Login.this, "Login Succesful", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Login.this, Profile.class);
                    intent.putExtra("email", "\""+email[0].substring(0, email[0].length()-4)+"\"");
                    startActivity(intent);
                } else
                    Toast.makeText(Login.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}
