package app.com.example.mahmoudshahen.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Profile extends AppCompatActivity {

    TextView hisName, hisEmail, hisNumber, hisBirth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        hisName = (TextView)findViewById(R.id.hisName);
        hisEmail = (TextView)findViewById(R.id.hisEmail);
        hisNumber = (TextView)findViewById(R.id.hisNumber);
        hisBirth = (TextView)findViewById(R.id.hisBirth);
        Intent intent = getIntent();
        final String email = intent.getStringExtra("email");
        Firebase.setAndroidContext(this);
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference(email);

        databaseReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                ArrayList<String> arrayList = new ArrayList<String>();

                for (com.google.firebase.database.DataSnapshot child : dataSnapshot.getChildren()) {
                    arrayList.add((String) child.getValue());

                }

                hisBirth.setText(arrayList.get(0));
                hisEmail.setText(email.substring(1, email.length()-1)+".com");
                hisName.setText(arrayList.get(1));
                hisNumber.setText(arrayList.get(2));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
