package app.com.example.mahmoudshahen.login;

/**
 * Created by mahmoud shahen on 10/6/2016.
 */
public class Data {
    String name;
    //String email;
    String number;
    String birthDate;

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public void setName(String name) {
        this.name = name;
    }

   /* public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }*/

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthdate) {
        this.birthDate = birthdate;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
